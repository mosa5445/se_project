const router = require('express').Router()

/**
 * controllers
 */
// ------------------------------------ //
const addNewEvent = require('../controllers/admin/Add_Event.controller')
const deleteEvent = require('../controllers/admin/Delete_Event.controller')
const getWH = require('../controllers/admin/Get_Working_Hours.controller')
const getUsers = require('../controllers/admin/Get_Users.controller ')
const addWH = require('../controllers/admin/Add_Working_Hours.controller')
// ------------------------------------ //

/**
  middlewares
 */
// ------------------------------------ //
const upload = require('../helpers/Upload')
// ------------------------------------ //


router.post('/event', upload.single('file'), addNewEvent.handle)
router.delete('/event', deleteEvent.handle)

router.get('/work-hours', getWH.handle)
router.post('/work-hours', addWH.handle)

router.get('/users', getUsers.handle)



module.exports = router