const jwt = require('jsonwebtoken')
const User = require('../../models/User')
const chalk = require('chalk');
const log = console.log;

class refreshTokenProccess {
    constructor() {
        return (async (req, res, next) => {
            let error = null
            const token = req.get('Authorization')
            const refresh_token = req.get('refresh_token')

            /**
             * find index of req refresh token
             */
            let index = await this.getIndexOfToken({
                user: req.user,
                token,
                refresh_token
            }, next)

            if (index == -1) {
                error = new Error()
                error.status = 401
                error.message = 'invalid refresh token'
                error.devMSG = ''
                return next(error)
            }

            /**
             * verify refresh_token and get payload
             */

            const payload = await this.verifyRefreshToken(refresh_token, next)


            /**
             *    this function is Doing this stuff
             *     1. check for equality between request token and the token that stored in refresh_token
             *     2. check for expiration of token
             *     3. remove refresh_token from db
             *     4. generate new token and refresh token
             */

            const tokenObject = await this.generateTokenObject({
                user: req.user,
                index,
                token,
                payload
            })
            if (tokenObject.status && tokenObject.status == 'notOk')
                return next(tokenObject.error)

            return res.json(tokenObject)
        })
    }

    async getIndexOfToken(data, next) {
        try {
            for (let i = 0; i <= data.user.tokens.length; i++)
                if (data.refresh_token == data.user.tokens[i])
                    return i
            return -1
        } catch (err) {
            log(chalk.whiteBright.bgRed.bold('controller => auth => refresh_token.auth.js => getIndexOfToken'))
            log(err)
            let error = new Error(`Internal Server Error`)
            error.status = 500
            error.userMSG = 'لطفا بعدا امتحان کنید'
            next(error)
            throw error
        }
    }

    verifyRefreshToken(token, next) {
        try {
            let decoded = jwt.verify(token, process.env.REFRESH_TOKEN_PRIVATE_KEY)
            return decoded
        } catch (err) {
            log(chalk.whiteBright.bgRed.bold('controller => auth => refresh_token.auth.js => verifyRefreshToken'))
            log(err)
            let error = new Error()
            error.message = 'refresh_token has been expired redirect user to login page'
            error.status = 401
            error.userMSG = ''
            next(error)
            throw error
        }
    }

    async generateTokenObject(data) {
        let error = null
        /**
         * Doing level 1
         */

        if (data.token != data.payload.token) { // 
            error = new Error()
            error.status = 401
            error.message = 'invalid refresh_token'
            error.userMSG = ''
            return {
                status: 'notOk',
                error
            }
        }

        /**
         * Doing Level 2
         */

        if (Date.now() < data.payload.expire) {
            error = new Error()
            error.status = 400
            error.message = "token doesen't expired by now"
            error.userMSG = ''
            return {
                status: 'notOk',
                error
            }
        }

        /**
         * Doing level 3
         */

        data.user.tokens.splice(data.index, 1)
        await data.user.save()

        /**
         * Doing level 4
         */
        let tokenObject = await data.user.generateToken()
        if (tokenObject.status != 'notOk')
            return tokenObject
        else
            return {
                status: 'notOk',
                error: tokenObject.error
            }
    }
}

module.exports = new refreshTokenProccess