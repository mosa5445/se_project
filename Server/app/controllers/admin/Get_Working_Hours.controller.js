const WH = require('../../models/WH')
const chalk = require('chalk');
const log = console.log;

exports.handle = async (req, res, next) => {
    try {
        const { userId } = req.query
        console.log(req.query)
        let result;
        if (userId)
            result = await WH.find({ employee: userId }).populate({ modle: 'User', path: 'employee', select: 'id name phoneNumber date time' })
        else
            result = await WH.find({}).populate({ modle: 'User', path: 'employee', select: 'id name phoneNumber date time' })

            
        
        return res.json(result)

    } catch (err) {
        log(chalk.whiteBright.bgRed.bold('controller => admin = Get_Working_Hours.controller.js'))
        log(err)
        let error = new Error(`Internal Server Error`)
        error.status = 500
        error.userMSG = 'لطفا بعدا امتحان کنید'
        res.status(500).json(error)
        throw error
    }
}