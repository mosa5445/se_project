const WH = require('../../models/WH')
const chalk = require('chalk');
const log = console.log;

exports.handle = async (req, res, next) => {
    try {
        const { userId, date, time } = req.body

        await WH.create({
            employee: userId,
            submitedBy: req.user.id,
            date,
            time
        })

        return res.send()

    } catch (err) {
        log(chalk.whiteBright.bgRed.bold('controller => admin = Add_Working_Hours.controller.js'))
        log(err)
        let error = new Error(`Internal Server Error`)
        error.status = 500
        error.userMSG = 'لطفا بعدا امتحان کنید'
        res.status(500).json(error)
        throw error
    }
}