const User = require('../../models/User')
const chalk = require('chalk');
const log = console.log;

exports.handle = async (req, res, next) => {
    try {
        const { userId, type } = req.query
        const query = User.find()
        if (type) query.where('role').equals(type)
        if (userId) query.where('id').equals(userId)
        const result = await query.exec()

        res.json(result)

    } catch (err) {
        log(chalk.whiteBright.bgRed.bold('controller => admin = Get_Working_Hours.controller.js'))
        log(err)
        let error = new Error(`Internal Server Error`)
        error.status = 500
        error.userMSG = 'لطفا بعدا امتحان کنید'
        res.status(500).json(error)
        throw error
    }
}