const chalk = require('chalk');
const log = console.log;

exports.handle = async (req, res, next) => {
    try {

        const { id ,  name , email , phoneNumber , role , createdAt} = req.user
        const info = { id ,  name , email , phoneNumber , role , createdAt}
        return res.json(info)
    } catch (err) {
        log(chalk.whiteBright.bgRed.bold('controller => profile = get_user_info.profile.js'))
        log(err)
        let error = new Error(`Internal Server Error`)
        error.status = 500
        error.userMSG = 'لطفا بعدا امتحان کنید'
        res.status(500).json(error)
        throw error
    }
}