const mongoose = require('mongoose')


const Register = new mongoose.Schema({
    phoneNumber: String,
    verifyCode: Number,
    verifyCodeExpire: Number,
    verifyStatus: {
        type: Boolean,
        default: false
    }

}, { timestamps: true, toJSON: { virtuals: true } })

module.exports = mongoose.model('register', Register)