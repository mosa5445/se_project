const mongoose = require('mongoose')
const bcrypt = require('bcryptjs')
const mongoosePaginate = require('mongoose-paginate-v2')
const jwt = require('jsonwebtoken')
const chalk = require('chalk');
const log = console.log;

const User = new mongoose.Schema({
  name: String,
  email: String,
  password: String,
  phoneNumber: String,
  verifyCode: Number,
  verifyCodeExpire: Number,
  tokens: [
    {
      type: String,
      expire: Number
    }
  ],
  role: {
    type: String,
    default: 'user'
  },
}, { timestamps: true, toJSON: { virtuals: true } })

/*
  Plugins
*/
User.plugin(mongoosePaginate)

/*
  Vituals
*/

// User.virtual('media', {
//   ref: 'media',
//   localField: '_id',
//   foreignField: 'uploader'
// })

/*
  Methods
*/

User.methods.hashPassword = async function (data) {
  const salt = bcrypt.genSaltSync(10)
  const hash = bcrypt.hashSync(data, salt)
  return hash
}

User.methods.checkPassword = async function (user, input) {
  if (!bcrypt.compareSync(input, user)) { return false }
  return true
}

User.methods.generateToken = async function () {
  // TODO check here for use _id or id , which one is correct? 

  try {
    /**
     * generate token and refresh token
     */

    const expire = Date.now() + 1000 * 60 * 60  // 1 hour
    const token = await jwt.sign({
      id: this._id
    }, process.env.JSON_WEB_TOKEN_PRIVATE_KEY, { expiresIn: `${expire} ms` }).toString()


    const refresh_token = await jwt.sign({
      id: this._id,
      token: 'Bearer ' + token,
      expire
    }, process.env.REFRESH_TOKEN_PRIVATE_KEY, { expiresIn: '30 days' }).toString()

    /**
     * save token in db
     */
    this.tokens.push(refresh_token)
    await this.save()

    return {
      token,
      refresh_token
    }
  } catch (err) {
    log(chalk.whiteBright.bgRed.bold('models => User.js'))
    log(err)
    let error = new Error(`Internal Server Error`)
    error.status = 500
    error.userMSG = 'لطفا بعدا امتحان کنید'

    return {
      status: 'notOk',
      error
    }
  }


}
module.exports = mongoose.model('user', User)
