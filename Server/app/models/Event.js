const mongoose = require('mongoose')


const Event = new mongoose.Schema({
    creator: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    title: String,
    content: String,
    date: String,
    image: String

}, { timestamps: true, toJSON: { virtuals: true } })

Event.index({ title: 'text', content: 'text' })

module.exports = mongoose.model('event', Event)