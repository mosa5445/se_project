const mongoose = require('mongoose')


const Post = new mongoose.Schema({
    userId: Number,
    id: Number,
    title: String,
    body: String

}, { timestamps: true, autoIndex: false, toJSON: { virtuals: true } })

Post.index({ '$**': 'text' })

module.exports = mongoose.model('post', Post)