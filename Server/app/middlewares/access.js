const User = require('../models/User')


exports.check = function (roles) {
    return function (req, res, next) {

        for (const role of roles) {
            if (req.user.role == role)
                return next()
        }

        let err = new Error(`access denied for ${req.user.role}`)
        err.status = 403
        err.userMSG = "دسترسی مجاز نیست "
        next(err)
    }
}