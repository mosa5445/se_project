const multer = require('multer')
const mkdirp = require('mkdirp')

try {
  const getDirImage = () => {
    const year = new Date().getFullYear()
    // const month = new Date().getMonth() + 1
    return `./public/uploads/images/${year}`
  }
  const getDirVideo = () => {
    const year = new Date().getFullYear()
    // const month = new Date().getMonth() + 1
    return `./public/uploads/videos/${year}`
  }

  const fileStorage = multer.diskStorage({
    destination: (req, file, cb) => {
      try {
        let dir
        if (file.mimetype === 'image/jpg' ||
          file.mimetype === 'image/png' ||
          file.mimetype === 'image/jpeg') {
          dir = getDirImage()
        } else {
          dir = getDirVideo()
        }

        mkdirp(dir, (err) => {
          if (err) { console.log('helpers/upload.js Image destinaition', err) }
          cb(null, dir)
        })
      } catch (err) {
        console.error('Err Location => helper/Upload  fileStorage => destination \n' + err)
        const Err = new Error(err.message)
        Err.status = 400
        Err.userMSG = 'خطایی رخ داده ، بعدا امتحان کنید'
        cb(Err, false)
      }
    },
    filename: (req, file, cb) => {
      // const filePath = getDirImage() + '/' + file.originalname
      cb(null, new Date().toISOString().replace(/:/g, '-') + '-' + file.originalname)
    }
  })

  /*
  Limit Format of Files
  */
  const fileFilter = (req, file, cb) => {
    try {
      if (
        file.mimetype === 'image/jpg' ||
        file.mimetype === 'image/png' ||
        file.mimetype === 'image/jpeg' ||
        file.mimetype === 'video/mp4' ||
        file.mimetype === 'video/m4a' ||
        file.mimetype === 'video/avi') {
        cb(null, true)
      } else {
        const Err = new Error('invalid file format')
        Err.status = 415
        Err.userMSG = 'فرمت فایل ارسال شده مجاز نیست'
        cb(Err, false)
      }
    } catch (err) {
      console.error('Err Location => helper/Upload  fileFilter\n' + err)
      const Err = new Error(err.message)
      Err.status = 400
      Err.userMSG = 'خطایی رخ داده ، بعدا امتحان کنید'
      cb(Err, false)
    }
  }

  var upload = multer({
    storage: fileStorage,
    fileFilter: fileFilter,
    limits: {
      fileSize: 1024 * 1024 * 400
    }
  })
} catch (err) {
  console.error('Err Location => helper/Upload \n' + err)
  const Err = new Error(err.message)
  Err.status = 400
  Err.userMSG = 'خطایی رخ داده ، بعدا امتحان کنید'
  throw Err
}

module.exports = upload
