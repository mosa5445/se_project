const validator = require('validator')
const chalk = require('chalk');
const log = console.log;

exports.validate = async (name, email, next) => {
    try {
        const errors = [];

        if (validator.default.isEmpty(email))
            errors.push({ type: 'email', err: 'ایمیل الزامیست' })

        else if (!validator.default.isEmail(email))
            errors.push({ type: 'email', err: 'ادرس ایمیل نامعتبر است' })

        if (validator.default.isEmpty(name))
            errors.push({ type: 'name', err: 'نام الزامیست' })


        if (!errors.length) {
            return {
                status: 'ok'
            }
        }

        const error = new Error()
        error.status = 400
        error.devMSG = ''
        error.userMSG = 'لطفا مقادیر وارد شده را مجدد بررسی کنید'
        error.errors = errors
        return {
            status: 'notOk',
            error
        }

    } catch (err) {
        log(chalk.whiteBright.bgRed.bold('validtor => sendSMS.validator.js'))
        log(err)
        let error = new Error(`Internal Server Error`)
        error.status = 500
        error.userMSG = 'لطفا بعدا امتحان کنید'
        next(error)
        throw error
    }
}