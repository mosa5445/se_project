const validator = require('validator')
const chalk = require('chalk');
const log = console.log;

exports.validate = async (email, password, name, next) => {
    try {
        const errors = [];

        if (!validator.default.isEmail(email))
            errors.push({ name: 'email', message: 'ایمیل نامعتبر' })

        if (!validator.default.isLength(password, { max: 20, min: 8 }))
            errors.push({ name: 'password', message: 'طول مجاز گذر واژه بین 8 تا 128 کاراکتر می باشد' })

        if (validator.default.isEmpty(name))
            errors.push({ name: 'name', message: '' })

        if (!errors.length) {
            return {
                status: 'ok'
            }
        }

        const error = new Error()
        error.status = 400
        error.devMSG = ''
        error.userMSG = 'لطفا مقادیر وارد شده را مجدد بررسی کنید'
        error.errors = errors
        return {
            status: 'notOk',
            error
        }

    } catch (err) {
        log(chalk.whiteBright.bgRed.bold('validtor => singup.validator.js'))
        log(err)
        let error = new Error(`Internal Server Error`)
        error.status = 500
        error.userMSG = 'لطفا بعدا امتحان کنید'
        next(error)
        throw error
    }
}